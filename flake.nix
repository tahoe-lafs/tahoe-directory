{
  description = "An implementation of the Tahoe-LAFS filesystem directory-like abstraction for managing collections of data objects.";

  inputs = {
    # Nix Inputs
    flake-utils.url = github:numtide/flake-utils;
    hs-flake-utils.url = "git+https://whetstone.private.storage/jcalderone/hs-flake-utils.git?ref=main";
    nixpkgs.follows = "hs-flake-utils/nixpkgs";
    tahoe-chk = {
      url = "git+https://whetstone.private.storage/PrivateStorage/tahoe-chk?ref=refs/tags/0.2.0.0";
      inputs.nixpkgs.follows = "hs-flake-utils/nixpkgs";
    };
    tahoe-ssk = {
      url = "git+https://whetstone.private.storage/PrivateStorage/tahoe-ssk?ref=refs/tags/0.3.0.0";
      inputs.nixpkgs.follows = "hs-flake-utils/nixpkgs";
      inputs.tahoe-chk.follows = "tahoe-chk";
    };
    tahoe-capabilities = {
      url = "git+https://whetstone.private.storage/PrivateStorage/tahoe-capabilities?ref=refs/tags/0.1.0.0";
      inputs.nixpkgs.follows = "hs-flake-utils/nixpkgs";
    };
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    hs-flake-utils,
    tahoe-chk,
    tahoe-ssk,
    tahoe-capabilities,
  }: let
    ulib = flake-utils.lib;
    ghcVersion = "ghc8107";
  in
    ulib.eachSystem ["x86_64-linux" "aarch64-darwin"] (system: let
      # Get a nixpkgs customized for this system
      pkgs = import nixpkgs {
        inherit system;
      };
      hslib = hs-flake-utils.lib {
        inherit pkgs;
        src = ./.;
        compilerVersion = ghcVersion;
        packageName = "tahoe-directory";
        hsPkgsOverrides = hfinal: hprev: {
          tahoe-chk = tahoe-chk.outputs.packages.${system}.default;
          tahoe-ssk = tahoe-ssk.outputs.packages.${system}.default;
          tahoe-capabilities = tahoe-capabilities.outputs.packages.${system}.default;
          haskellLib = pkgs.haskell.lib;
        };
      };
    in {
      checks = hslib.checks {};
      devShells = hslib.devShells {
        shellHook = ''
          nix run .#write-cabal-project
        '';
        extraBuildInputs = pkgs:
          with pkgs; [
            zlib
          ];
      };
      packages = hslib.packages {};
      apps.hlint = hslib.apps.hlint {};

      apps.write-cabal-project = hslib.apps.write-cabal-project {
        localPackages = {
          "tahoe-chk" = tahoe-chk.sourceInfo.outPath;
          "tahoe-ssk" = tahoe-ssk.sourceInfo.outPath;
        };
      };

      # Using the working directory of `nix run`, do a build with cabal and
      # then run the test suite.
      apps.cabal-test = hslib.apps.cabal-test {
        preBuild = ''
          nix run .#write-cabal-project
        '';
      };

      apps.release = hslib.apps.release {};
    });
}
