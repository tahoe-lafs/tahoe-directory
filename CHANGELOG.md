# Revision history for tahoe-directory

## 0.1.0.0 -- 2023-08-17

* First version. Released on an unsuspecting world.
* Support for parsing and serializing CHK and SDMF directory capability strings.
* Support for parsing and serializing directories
  (lists of name, capability, metadata tuples).
