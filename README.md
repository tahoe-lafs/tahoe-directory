# tahoe-directory

[![Most recent pipeline status badge](https://gitlab.com/tahoe-lafs/tahoe-directory/badges/main/pipeline.svg)](https://gitlab.com/tahoe-lafs/tahoe-directory/-/pipelines?scope=all&ref=main)

## What is it?

Tahoe-Directory is a Haskell implementation of the [Tahoe-LAFS](https://tahoe-lafs.org/) filesystem directory-like abstraction for managing collections of data objects.
This includes a parser and serializer for the persisted form of these collections.
It also includes Haskell-friendly abstract representations of and operations on the data.
It aims for bit-for-bit compatibility with the original Python implementation.

It will not include an implementation of any network protocol for transferring directories.
However, its APIs are intended to be easy to integrate with such an implementation.

### What is the current state?

* Verify and read CHK directory capability strings can be parsed and serialized.
* Verify, read, and write SDMF directory capability strings can be parsed and serialized.
* Directories themselves can be parsed and serialized.
  * However, capability strings and metadata in directory entries are left as byte strings.

## Why does it exist?

A Haskell implementation can be used in places the original Python implementation cannot be
(for example, runtime environments where it is difficult to have a Python interpreter).
Additionally,
with the benefit of the experience gained from creating and maintaining the Python implementation,
a number of implementation decisions can be made differently to produce a more efficient, more flexible, simpler implementation and API.
Also,
the Python implementation claims no public library API for users outside of the Tahoe-LAFS project itself.
